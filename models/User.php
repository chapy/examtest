<?php

	namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;

	class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
	{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

	public function rules()
    {
        return [
            [['username', 'password', 'auth_key','firstname','lastname','statusId','levelId'], 'string', 'max' => 255],
			[['username', 'password'], 'required'],
            [['username'], 'unique']			
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'auth_key' => 'Auth Key',
			'firstname' => 'First Name',
			'lastname' => 'Last Name',
			'levelId' => 'levelId',
        ];
    }	
	
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}

	/**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
		throw new NotSupportedException('You can only login
							by username/password pair for now.');
    }

 
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
         return $this->getAuthKey() === $authKey;
    }	

	public function validatePassword($password)
	{
		return $this->isCorrectHash($password, $this->password); 
	}

	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}

    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->auth_key = Yii::$app->security->generateRandomString(32);

        return $return;
    }
	public static function getlevels()
	{
		$levels = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $levels;						
	}
	
	public static function getlevelswithalllevels()
	{
		$levels = self::getlevels();
		$levels[-1] = 'All levels';
		$levels = array_reverse ( $levels, true );
		return $levels;	
	
}
	}