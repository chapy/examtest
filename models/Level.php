<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;	


/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property string $name
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
		public static function getlevels()
	{
		$levels = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $levels;						
	}
	
	public static function getlevelswithalllevels()
	{
		$levels = self::getlevels();
		$levels[-1] = 'All levels';
		$levels = array_reverse ( $levels, true );
		return $levels;	
	
}
}
