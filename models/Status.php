<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;	

/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $name
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	public static function getstatuses()
	{
		$statuses = ArrayHelper::
					map(self::find()->all(), 'id', 'name');
		return $statuses;						
	}
	
	public static function getstatuswithallstatuses()
	{
		$statuses = self::getstatuses();
		$statuses[-1] = 'All statuses';
		$statuses = array_reverse ( $statuses, true );
		return $statuses;	
	
}
}
