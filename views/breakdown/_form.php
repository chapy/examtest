<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Level;
use app\models\Status;


/* @var $this yii\web\View */
/* @var $model app\models\Breakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="breakdown-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'levelId')->textInput()-> 
				dropDownList(level::getlevels()) ?>  

   <!-- <?= $form->field($model, 'statusId')->textInput()->
						dropDownList(Status::getStatuses()) ?>  -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
